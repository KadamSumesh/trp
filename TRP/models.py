from django.db import models
from django.urls import reverse
# Create your models here.


class upload_file(models.Model):

    file_upload = models.FileField()

    def get_absolute_url(self):
        return reverse('trp:index')