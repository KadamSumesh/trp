import pandas as pd
import numpy as np
import json
import os

with open(os.getcwd() + '/media/Balances_input.txt', 'r') as inputfile:
    input_string = str(inputfile.read())

input_json = json.loads(input_string)

def compute_external_dataframe():
    temp_ledger = pd.read_excel(os.getcwd() + "/media/" +input_json['external_ledger']).dropna(how='all').reset_index(drop=True)
    header = temp_ledger.iloc[5]
    temp_ledger.drop(temp_ledger.index[:6], inplace=True)
    temp_ledger.index = range(len(temp_ledger.index))
    header.iloc[-1] = input_json['externalTableFundCol']
    temp_ledger.columns = header
    return temp_ledger


def compute_local_dataframe(level_name, level_keys):
    key = [0]
    for x in range(0, len(local_ledger[level_name])):
        if (key[0] < len(level_keys) - 1):
            if (local_ledger[level_name][x] == level_keys[key[0] + 1]):
                key[0] = key[0] + 1
            local_ledger.loc[x, level_name] = level_keys[key[0]]
        else:
            local_ledger.loc[x, level_name] = level_keys[key[0]]


def checkExternal(partnerName):
    if (partnerName == 'GS'):
        return compute_external_dataframe()
    else:
        return pd.read_excel(input_json['external_ledger']).dropna(how='all').reset_index()


external_ledger = checkExternal(input_json['partnerName'])

select_ledger = external_ledger.loc[external_ledger[input_json['externalTableFundCol']] == input_json['fundName']]

local_ledger = pd.read_excel(os.getcwd() + "/media/" + input_json['local_ledger']).reset_index()

level0_keys = local_ledger['level_0'].dropna().reset_index()['level_0']
level1_keys = local_ledger['level_1'].dropna().reset_index()['level_1']
level2_keys = local_ledger['level_2'].dropna().reset_index()['level_2']

compute_local_dataframe('level_0', level0_keys)
compute_local_dataframe('level_1', level1_keys)
compute_local_dataframe('level_2', level2_keys)

local_ledger = local_ledger[local_ledger['Type'] == 'Opening Balance'].reset_index(drop=True)
custom_ledger = local_ledger.loc[(local_ledger['level_1'] == input_json['partnerName'])]

common_ledger = pd.merge(select_ledger, custom_ledger, left_on=['Currency'], right_on=['level_2'], how='outer',
                         indicator=True).replace(np.nan, '', regex=True)
matched_entries_external_ledgers = common_ledger[common_ledger['_merge'] == 'both'].iloc[:,
                                   0:len(external_ledger.columns)]
unmatched_entries_external_ledgers = common_ledger[common_ledger['_merge'] == 'left_only'].iloc[:,
                                     0:len(external_ledger.columns)]
unmatched_entries_local_ledgers = common_ledger[common_ledger['_merge'] == 'right_only'].iloc[:,
                                  len(external_ledger.columns): len(external_ledger.columns) + len(
                                      local_ledger.columns)]

matched_entries_external_ledgers.to_excel(os.getcwd() + "/media/Match_Transaction_Balances.xlsx")
unmatched_entries_local_ledgers.to_excel(os.getcwd() + "/media/Partners_Exception_Balances(Tie).xlsx")
unmatched_entries_local_ledgers.to_excel(os.getcwd() + "/media/Traders_Exception_Balances(Tie).xlsx")