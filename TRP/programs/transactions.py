import pandas as pd
import numpy as np
import json
import os

currency_dict = {'CANADIAN DOLLAR' : 'CAD', 'EURO' : 'EUR', 'U S DOLLAR' : 'USD', 'JAPANESE YEN' : 'JPY', 'UK POUND STERLING' : 'GBP'}


with open(os.getcwd() + '/media/Transactions_input.txt', 'r') as inputfile:
    input_string = str(inputfile.read())

input_json = json.loads(input_string)


trp_left = input_json['trp_left']
trp_right = input_json['trp_right']


external_ledger = pd.read_excel(os.getcwd() + "/media/" + input_json['external_ledger'])
local_ledger = pd.read_excel(os.getcwd() + "/media/" + input_json['local_ledger'])


accountType = input_json['accountType']
currency_Type = input_json['currency_Type']
currencyType = input_json['currencyType']


isCompany = input_json['isCompany']
isCurrency = input_json['isCurrency']

for key, value in currency_dict.items():
    external_ledger.loc[external_ledger['Currency'] == key, 'Currency'] = value
    local_ledger.loc[local_ledger['Ccy'] == key, 'Ccy'] = value


select_ledger = external_ledger.loc[external_ledger[currencyType] == isCurrency]
custom_ledger = local_ledger.loc[(local_ledger[accountType] == isCompany) & (local_ledger[currency_Type] == isCurrency)]

common_ledger = pd.merge(select_ledger, custom_ledger, left_on = trp_left, right_on = trp_right , how = 'outer', indicator = True).replace(np.nan, '', regex=True)
matched_entries = common_ledger[common_ledger['_merge'] == 'both'].iloc[:, 0:(len(common_ledger.columns)-1)]
matched_entries_external_ledgers = common_ledger[common_ledger['_merge'] == 'both'].iloc[:, 0:len(external_ledger.columns)]
unmatched_entries_external_ledgers = common_ledger[common_ledger['_merge'] =='left_only'].iloc[:, 0:len(external_ledger.columns)]
unmatched_entries_local_ledgers = common_ledger[common_ledger['_merge'] =='right_only'].iloc[:, len(external_ledger.columns) : len(external_ledger.columns) + len(local_ledger.columns)]

matched_entries_external_ledgers.to_excel(os.getcwd() + "/media/Match_Records_Transactions.xlsx")
unmatched_entries_external_ledgers.to_excel(os.getcwd() + "/media/Partners_Exception_Transactions(Tie).xlsx")
unmatched_entries_local_ledgers.to_excel(os.getcwd() + "/media/Traders_Exception_Transactions(Tie).xlsx")