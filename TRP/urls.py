from django.conf.urls import url
from . import views

app_name = 'trp'

urlpatterns = [
    url(r'^$', views.IndexPage.as_view(), name='index'),
    url(r'^upload/', views.UploadFile.as_view(), name='upload'),
    url(r'^scratchpad/', views.scratch_pad, name='scratchpad'),
    url(r'^analysisquery/', views.analysis_query, name='analysisquery'),
    url(r'^cleandatabase/', views.clean_database, name='cleandatabase'),
]