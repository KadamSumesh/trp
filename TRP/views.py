from django.shortcuts import render, redirect, reverse
import subprocess
# Create your views here.

from django.http import HttpResponseRedirect
from django.views.generic import CreateView
from TRP.models import upload_file
from django.views import generic
import pandas as pd
import os


#View for Index Page
class IndexPage(generic.ListView):
    context_object_name = 'model_list'
    template_name = "TRP/index.html"

    def get_queryset(self):
        return upload_file.objects.all()

    def get_context_data(self, **kwargs):
        context = super(IndexPage, self).get_context_data(**kwargs)
        context['Model_Object'] = upload_file.objects.all()
        context['File_Object'] = os.listdir(os.getcwd() + "/media/")
        return context

#View for Upload File Page
class UploadFile(CreateView):
    model = upload_file
    fields = ['file_upload']
    template_name = "TRP/upload.html"


#View for Upload File Page
def scratch_pad(request):
    scratch_pad_file = subprocess.check_output("cat " + os.getcwd() + "/media/Scratchpad_input.txt", shell=True).decode('ascii')
    p = os.getcwd() + '/media/' + scratch_pad_file
    df = pd.read_excel(p)
    return render(request, 'TRP/scratchpad.html', {'content': df.to_json()})


#View for Analysis Query Page
def analysis_query(request):
    subprocess.call("python ./TRP/programs/balances.py", shell=True)
    subprocess.call("python ./TRP/programs/transactions.py", shell=True)

    bal_matchdf = pd.read_excel(os.getcwd() + '/media/Match_Transaction_Balances.xlsx')
    bal_prexpdf = pd.read_excel(os.getcwd() + '/media/Partners_Exception_Balances(Tie).xlsx')
    bal_trexpdf = pd.read_excel(os.getcwd() + '/media/Traders_Exception_Balances(Tie).xlsx')

    tr_matchdf = pd.read_excel(os.getcwd() + '/media/Match_Records_Transactions.xlsx')
    tr_prexpdf = pd.read_excel(os.getcwd() + '/media/Partners_Exception_Transactions(Tie).xlsx')
    tr_trexpdf = pd.read_excel(os.getcwd() + '/media/Traders_Exception_Transactions(Tie).xlsx')

    return render(request, 'TRP/analysisquery.html', {'matched_records': bal_matchdf.to_json(), 'PRExceptions': bal_prexpdf.to_json(), 'TRExceptions': bal_trexpdf.to_json(),
                                                      'matched_entries': tr_matchdf.to_json(), 'PRTies': tr_prexpdf.to_json(), 'TRTies': tr_trexpdf.to_json()})


def clean_database(request):
    subprocess.call("yes yes |python manage.py flush", shell=True)
    subprocess.call('rm ' + os.getcwd() + '/media/Balances_input.txt', shell= True)
    subprocess.call('rm ' + os.getcwd() + '/media/Transactions_input.txt', shell=True)
    return HttpResponseRedirect(reverse('trp:index'))